<?php

class Cart_model extends MY_Model
{
    protected $_table_name = 'tbl_carts';
    protected $_primary_key = '';

    public function get_list($order_id)
    {
        $this->db->select('a.product_name, a.product_price, b.quantity, b.subtotal');
        $this->db->from('tbl_products AS a');
        $this->db->join('tbl_carts AS b', 'a.product_id = b.product_id');
        $this->db->where('b.order_id', $order_id);

        return $this->db->get()->result();
    }
}