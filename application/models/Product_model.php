<?php

class Product_model extends MY_Model
{
    protected $_table_name = 'tbl_products';
    protected $_primary_key = 'product_id';

    public function get_list()
    {
        return $this->db->get($this->_table_name)->result();
    }

    public function get_details($product_id)
    {
        $this->db->where($this->_primary_key, $product_id);

        return $this->db->get($this->_table_name)->row();
    }

    public function get_image($product_id)
    {
        $this->db->select('product_image');
        $this->db->where($this->_primary_key, $product_id);

        return $this->db->get($this->_table_name)->row();
    }
}