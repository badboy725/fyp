<?php

class User_model extends MY_Model
{
    protected $_table_name = 'tbl_users';
    protected $_primary_key = 'user_id';

    public function check_username($username)
    {
        return $this->db->where('username', $username)->get($this->_table_name)->data_seek();
    }

    public function login($data)
    {
        $this->db->select('user_id, username, fullname, email, handphone_number, gender, role_keyword');
        $this->db->from($this->_table_name);
        $this->db->where($data);

        return $this->db->get()->row();
    }

    public function check_password($data)
    {
        return $this->db->where($data)->get($this->_table_name)->data_seek();
    }

    public function get_staff_list()
    {
        $this->db->select('user_id, fullname, email, handphone_number, gender');
        $this->db->from($this->_table_name);
        $this->db->where('role_keyword', ROLE_STAFF);

        return $this->db->get()->result();
    }

    public function get_details($user_id)
    {
        $this->db->select('user_id, username, fullname, email, handphone_number, gender');
        $this->db->from($this->_table_name);
        $this->db->where($this->_primary_key, $user_id);

        return $this->db->get()->row();
    }
}
