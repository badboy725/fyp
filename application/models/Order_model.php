<?php

class Order_model extends MY_Model
{
    protected $_table_name = 'tbl_orders';
    protected $_primary_key = 'order_id';

    public function get_list()
    {
        $this->db->select('a.order_id, a.total_payment, a.address, a.status');
        $this->db->select("
            CASE 
                WHEN payment_type = 1 THEN 'Cash On Delivery'
                ELSE 'Online Banking'
            END AS payment_type
        ");
        $this->db->select("DATE_FORMAT(a.order_date, '%d-%b-%Y %H:%i:%s') AS order_date");
        $this->db->from("{$this->_table_name} AS a");

        if($this->session->userdata('role_keyword') == ROLE_USER){
            $this->db->where('user_id', $this->session->userdata('user_id'));
        }else if($this->session->userdata('role_keyword') == ROLE_ADMIN){
            $this->db->select('fullname');
            $this->db->join('tbl_users AS b', 'a.user_id = b.user_id');
        }

        if($this->session->userdata('role_keyword') == ROLE_STAFF){
            $this->db->where('staff_user_id', $this->session->userdata('user_id'));
        }

        $this->db->order_by('order_date', 'desc');

        return $this->db->get()->result();
    }

    public function get_task_list()
    {
        $this->db->select('a.order_id, a.total_payment, a.address, a.status');
        $this->db->select("
            CASE 
                WHEN payment_type = 1 THEN 'Cash On Delivery'
                ELSE 'Online Banking'
            END AS payment_type
        ");
        $this->db->select("DATE_FORMAT(a.order_date, '%d-%b-%Y %H:%i:%s') AS order_date");
        $this->db->from("{$this->_table_name} AS a");

        $this->db->where('staff_user_id', $this->session->userdata('user_id'));
        $this->db->where('status', 2);
        $this->db->order_by('order_date', 'desc');

        return $this->db->get()->result();
    }

    public function get_details($order_id)
    {
        $this->db->select('a.total_payment, a.address, a.payment_type, a.payment_receipt, a.status');
        $this->db->select('b.fullname, b.handphone_number, b.gender');
        $this->db->select("DATE_FORMAT(a.order_date, '%d-%b-%Y %H:%i:%s') AS order_date");
        $this->db->from('tbl_orders AS a');
        $this->db->join('tbl_users AS b', 'a.staff_user_id = b.user_id', 'left');
        $this->db->where('order_id', $order_id);

        return $this->db->get()->row();
    }
}