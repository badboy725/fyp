<?php

class Shopping_cart extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cart_model');
        $this->load->model('order_model');
    }

    public function index()
    {
        $cart['list'] = $this->cart->contents();
        $cart['total'] = $this->cart->total();

        $data['content'] = $this->load->view('cart/list', $cart, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function add()
    {
        $cart_data = array(
            'id'       => $this->input->post('product_id'),
            'name'     => $this->input->post('product_name'),
            'price'    => $this->input->post('product_price'),
            'qty'      => $this->input->post('quantity'),
            'subtotal' => $this->input->post('subtotal')
        );

        $this->cart->insert($cart_data);

        $total = count($this->cart->contents());
        $this->output->set_content_type('application/json')->set_output(json_encode($total));
    }

    public function edit($row_id)
    {
        if (!empty($_POST)) {
            if ($this->_save_edit_cart($row_id)) {
                redirect('shopping_cart');
            }
        }

        $cart['data'] = $this->cart->get_item($row_id);
        if (empty($cart['data'])) {
            redirect('shopping_cart');
        }

        $data['content'] = $this->load->view('cart/form', $cart, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _save_edit_cart()
    {
        $cart_data = array(
            'rowid'   => $this->input->post('row_id'),
            'qty'      => $this->input->post('quantity'),
            'subtotal' => $this->input->post('subtotal')
        );

        return $this->cart->update($cart_data);
    }

    public function delete($row_id)
    {
        $this->cart->remove($row_id);

        redirect('shopping_cart');
    }

    public function confirm()
    {
        if (!empty($_POST)) {
            $order_id = $this->_save_confirm();

            if(!empty($order_id)){
                redirect("order/receipt/{$order_id}");
            }
        }

        $cart['list'] = $this->cart->contents();
        $cart['total'] = $this->cart->total();

        $data['content'] = $this->load->view('cart/confirm', $cart, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _save_confirm()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'payment_type',
                'label' => 'Payment Type',
                'rules' => 'required'
            )
        ));

        if ($this->input->post('payment_type') == 2 && empty($_FILES['payment_receipt']['name'])) {
            $this->form_validation->set_rules('payment_receipt', 'Payment Receipt', 'required');
        }

        if ($this->form_validation->run() === TRUE) {
            $order_data = $this->input->post();
            $order_data['user_id'] = $this->session->userdata('user_id');
            $order_data['total_payment'] = $this->cart->total();
            $order_data['payment_receipt'] = empty($_FILES['payment_receipt']['name']) ? NULL : $this->_get_receipt_file();
            $order_data['order_date'] = date('Y-m-d H:i:s');

            $cart_data['order_id'] = $this->order_model->save($order_data);

            foreach ($this->cart->contents() as $value) {
                $cart_data['product_id'] = $value['id'];
                $cart_data['quantity'] = $value['qty'];
                $cart_data['subtotal'] = $value['subtotal'];

                $this->cart_model->save($cart_data);
            }

            $this->cart->destroy();

            return $cart_data['order_id'];
        }
    }

    private function _get_receipt_file()
    {
        $config = array(
            'allowed_types' => 'jpeg|jpg|png|pdf',
            'file_name' => 'payment_receipt',
        );

        return $this->_get_upload_file_path('payment_receipt', $config);
    }
}
