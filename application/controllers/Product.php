<?php

class Product extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function index()
    {
        $template_data['tab_active'] = 1;

        $list['list'] = $this->product_model->get_list();

        $template_data['tab_view'] = $this->load->view('product/list', $list, TRUE);
        $data['content'] = $this->load->view('product/product_template', $template_data, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function form($product_id = NULL)
    {
        if (!empty($_POST)) {
            if ($this->_save_form($product_id)) {
                redirect('product');
            }
        }

        $template_data['tab_active'] = 2;

        $form['data'] = $this->product_model->get_details($product_id);
        $template_data['tab_view'] = $this->load->view('product/form', $form, TRUE);
        $data['content'] = $this->load->view('product/product_template', $template_data, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _save_form($product_id)
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'product_name',
                'label' => 'Product Name',
                'rules' => 'trim|required|max_length[100]'
            ),
            array(
                'field' => 'product_price',
                'label' => 'Product Price',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'product_caption',
                'label' => 'Product Caption',
                'rules' => 'trim|required|max_length[255]'
            )
        ));

        if (empty($product_id) && empty($_FILES['product_image']['name'])) {
            $this->form_validation->set_rules('product_image', 'Product Image', 'required');
        }

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(array('product_name', 'product_price', 'product_caption'));
            if (!empty($_FILES['product_image']['name'])) {
                if (!empty($product_id)) {
                    $this->_remove_image_from_dir($product_id);
                }

                $data['product_image'] = $this->_get_image_file();
            }

            return $this->product_model->save($data, $product_id);
        }
    }

    private function _remove_image_from_dir($product_id)
    {
        $data = $this->product_model->get_image($product_id);

        if (file_exists(FCPATH . $data->product_image)) {
            unlink(FCPATH . $data->product_image);
        }
    }

    private function _get_image_file()
    {
        $config = array(
            'allowed_types' => 'jpeg|jpg|png',
            'file_name'     => strtolower($this->input->post('product_name')),
        );

        return $this->_get_upload_file_path('product_image', $config);
    }

    public function delete($product_id)
    {
        $this->_remove_image_from_dir($product_id);

        $this->product_model->delete($product_id);

        redirect('product');
    }

    public function get_product_details()
    {
        $data = $this->product_model->get_details($this->input->post('product_id'));

        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}
