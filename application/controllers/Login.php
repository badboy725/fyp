<?php

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index()
    {
        $data['content'] = $this->load->view('login/home', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function login()
    {
        if (!empty($_POST)) {
            $this->_login_form();
        }

        $data['content'] = $this->load->view('login/login', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _login_form()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post();
            $data['password'] = $this->_get_hash_password($data['password']);

            $user_data = $this->user_model->login($data);

            if (empty($user_data)) {
                $message = array(
                    'type'    => 'danger',
                    'message' => 'Username or Password is not match.'
                );
                $this->session->set_flashdata('message', $message);

                return FALSE;
            } else {
                $this->_set_session($user_data);

                redirect();
            }
        }
    }

    public function register()
    {
        if (!empty($_POST)) {
            if ($this->_register_form()) {
                redirect('login');
            }
        }

        $data['content'] = $this->load->view('login/register', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _register_form()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|min_length[10]|max_length[11]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|callback_check_username'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'confirmation_password',
                'label' => 'Confirmation Password',
                'rules' => 'trim|required|matches[password]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(array('fullname', 'email', 'handphone_number', 'gender', 'username', 'password'));
            $data['password'] = $this->_get_hash_password($data['password']);
            $data['role_keyword'] = ROLE_USER;

            return $this->user_model->save($data);
        }
    }

    public function menu()
    {
        $this->load->model('product_model');

        $menu['list'] = $this->product_model->get_list();
        $data['content'] = $this->load->view('menu/menu', $menu, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function logout()
    {
        $this->session->sess_destroy();

        redirect();
    }
}
