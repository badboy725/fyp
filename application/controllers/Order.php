<?php

class Order extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cart_model');
        $this->load->model('order_model');
        $this->load->model('user_model');
    }

    public function index()
    {
        $order['list'] = $this->order_model->get_list();

        $data['content'] = $this->load->view('order/list', $order, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function receipt($order_id)
    {
        if(!empty($_POST)){
            if($this->_assign_staff($order_id)){
                redirect("order/receipt/{$order_id}");
            }
        }

        $order['data'] = $this->order_model->get_details($order_id);
        $order['list'] = $this->cart_model->get_list($order_id);
        $order['staff_list'] = $this->user_model->get_staff_list();

        $data['content'] = $this->load->view('order/receipt', $order, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _assign_staff($order_id)
    {
        $this->form_validation->set_rules('staff_user_id', 'Staff to assign', 'required');

        if($this->form_validation->run() === TRUE){
            $data = array(
                'staff_user_id' => $this->input->post('staff_user_id'),
                'status' => 2
            );

            return $this->order_model->save($data, $order_id);
        }
    }
}