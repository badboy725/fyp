<?php

class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index()
    {
        if (!empty($_POST) && isset($_POST)) {
            if (isset($_POST['update_profile_btn'])) {
                $result = $this->_update_profile();
            } else if (isset($_POST['change_password_btn'])) {
                $result = $this->_change_password();
            }

            if ($result) {
                $message['type'] = TRUE;
                $message['message'] = isset($_POST['update_profile_btn']) ? 'Successfully Update Profile.' : 'Successfully Change Password.';
                $this->session->set_flashdata('message', $message);

                redirect('profile');
            }
        }

        $data['content'] = $this->load->view('profile/form', NULL, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _change_password()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|required|callback_check_password'
            ),
            array(
                'field' => 'new_password',
                'label' => 'New Password',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'confirmation_password',
                'label' => 'Confirmation Password',
                'rules' => 'trim|required|matches[new_password]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data['password'] = $this->_get_hash_password($this->input->post('new_password'));
            return $this->user_model->save($data, $this->session->userdata('user_id'));
        }
    }

    private function _update_profile()
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|min_length[10]|max_length[11]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ),
            array(
                'field' => 'personal_password_confirmation',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|callback_check_password'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(array('fullname', 'email', 'handphone_number', 'gender'));
            $this->user_model->save($data, $this->session->userdata('user_id'));

            $user_data = $this->user_model->login($data);
            $this->_set_session($user_data);

            return TRUE;
        }
    }

    public function check_password($password)
    {
        $where = array(
            'user_id' => $this->session->userdata('user_id'),
            'password' => $this->_get_hash_password($password)
        );

        if ($this->user_model->check_password($where) === FALSE) {
            $this->form_validation->set_message('check_password', 'Password is incorrect');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
