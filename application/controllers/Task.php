<?php

class Task extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_model');
    }

    public function index()
    {
        $order['list'] = $this->order_model->get_task_list();

        $data['content'] = $this->load->view('order/task_list', $order, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function delivered($order_id)
    {
        $this->order_model->save(array('status' => 3), $order_id);

        redirect('task');
    }
}