<?php

class Staff extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index()
    {
        $template_data['tab_active'] = 1;

        $list['list'] = $this->user_model->get_staff_list();

        $template_data['tab_view'] = $this->load->view('staff/list', $list, TRUE);
        $data['content'] = $this->load->view('staff/staff_template', $template_data, TRUE);
        $this->load->view('html_layout', $data);
    }

    public function form($user_id = NULL)
    {
        if (!empty($_POST)) {
            if ($this->_save_form($user_id)) {
                redirect('staff');
            }
        }

        $template_data['tab_active'] = 2;

        $form['data'] = $this->user_model->get_details($user_id);
        $template_data['tab_view'] = $this->load->view('staff/form', $form, TRUE);
        $data['content'] = $this->load->view('staff/staff_template', $template_data, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _save_form($user_id)
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|min_length[10]|max_length[11]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(array('fullname', 'email', 'handphone_number', 'gender'));
            $data['role_keyword'] = ROLE_STAFF;

            $insert_id = $this->user_model->save($data, $user_id);

            if(empty($user_id)){
                $this->_send_staff_email($insert_id);
            }

            return TRUE;
        }
    }

    public function delete($user_id)
    {
        $this->user_model->delete($user_id);

        redirect('staff');
    }

    private function _send_staff_email($user_id)
    {
        $data = $this->user_model->get_details($user_id);

        $message = 'Hi {fullname},<br/>
                    Thank to apply to be our staff. <br/>
                    Below are the link to register your account. <br/>
                    <a href="' . site_url("staff/register/{$data->user_id}") . '">Staff Registration</a> <br /><br />
                    Contact Us, if you have any problem.';

        $message = str_replace('{fullname}', $data->fullname, $message);

        $userdata = array(
            'to' => $data->email,
            'subject' => 'Staff Registration',
            'message' => $message
        );

        return $this->_send_mail($userdata);
    }

    public function register($user_id)
    {
        if (!empty($_POST)) {
            if ($this->_register_form($user_id)) {
                redirect('login');
            }
        }

        $data['data'] = $this->user_model->get_details($user_id);

        if(empty($data['data'])){
            redirect();
        }

        $data['content'] = $this->load->view('staff/register', $data, TRUE);
        $this->load->view('html_layout', $data);
    }

    private function _register_form($user_id)
    {
        $this->form_validation->set_rules(array(
            array(
                'field' => 'fullname',
                'label' => 'Full Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'handphone_number',
                'label' => 'Handphone Number',
                'rules' => 'trim|required|integer|min_length[10]|max_length[11]'
            ),
            array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|callback_check_username'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|alpha_numeric'
            ),
            array(
                'field' => 'confirmation_password',
                'label' => 'Confirmation Password',
                'rules' => 'trim|required|matches[password]'
            )
        ));

        if ($this->form_validation->run() === TRUE) {
            $data = $this->input->post(array('fullname', 'email', 'handphone_number', 'gender', 'username', 'password'));
            $data['password'] = $this->_get_hash_password($data['password']);

            return $this->user_model->save($data, $user_id);
        }
    }
}