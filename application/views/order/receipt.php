<div class="row m-0 mt-2 mb-2">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="bg-light" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
            <form role="form" method="post" autocomplete="off">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <table class="table">
                            <tr>
                                <th colspan="4">Receipt</th>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <th>Price (RM)</th>
                                <th>Quantity</th>
                                <th>Subtotal (RM)</th>
                            </tr>
                            <?php foreach ($list as $value) : ?>
                                <tr>
                                    <td><?php echo $value->product_name; ?></td>
                                    <td><?php echo number_format($value->product_price, 2); ?></td>
                                    <td><?php echo $value->quantity; ?></td>
                                    <td><?php echo number_format($value->subtotal, 2); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <th colspan="3" class="text-right">Total (RM)</th>
                                <td><?php echo number_format($data->total_payment, 2); ?></td>
                            </tr>
                        </table>

                        <div class="form-group">
                            <label>Address</label>
                            <p><?php echo $data->address; ?></p>
                        </div>

                        <div class="form-group">
                            <label>Payment Type :</label>
                            <select class="form-control" name="payment_type" id="payment_type" disabled>
                                <option value="">Please Select</option>
                                <option value="1" <?php echo set_select('payment_type', 1, 1 == $data->payment_type); ?>>Cash On Delivery</option>
                                <option value="2" <?php echo set_select('payment_type', 2, 2 == $data->payment_type); ?>>Online Banking</option>
                            </select>
                        </div>

                        <div class="form-group <?php echo $data->payment_type == 2 ? '' : 'my_hidden' ?>">
                            <label>Payment Receipt :</label> <a href="<?php echo base_url($data->payment_receipt); ?>" target="_blank">Payment Receipt</a>
                        </div>

                        <div class="form-group">
                            <label>Date :</label> <?php echo $data->order_date; ?>
                        </div>
                    
                        <?php if(empty($data->staff_user_id) && $data->status == 1 && $this->session->userdata('role_keyword') == ROLE_ADMIN): ?>
                            <div class="form-group <?php echo form_has_error('staff_user_id'); ?>">
                                <label for="staff_user_id">Staff to assign :</label>
                                <select class="form-control <?php echo form_has_error('staff_user_id'); ?>" name="staff_user_id" id="staff_user_id">
                                    <option value="">Please Select</option>
                                    <?php foreach($staff_list as $value) : ?>
                                        <option value="<?php echo $value->user_id; ?>"><?php echo $value->fullname; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo form_error_label('staff_user_id'); ?>
                            </div>

                            <div class="form-group my_right">
                                <button type="submit" class="btn btn-info btn-flat">Assign</button>
                                <button type="button" class="btn btn-info btn-flat" onclick="window.location.href='<?php echo site_url('order'); ?>'">Back</button>
                            </div>
                        <?php elseif($data->status == 3): ?>
                            <div class="form-group">
                                <table class="table">
                                    <tr>
                                        <th>Staff Name</th>
                                        <th>Phone Number</th>
                                        <th>Gender</th>
                                        <?php if($data->status == 3): ?>
                                            <th>Status</th>
                                        <?php endif; ?>
                                    </tr>
                                    <tr>
                                        <td><?php echo $data->fullname; ?></td>
                                        <td><?php echo $data->handphone_number; ?></td>
                                        <td><?php echo $data->gender == 'M' ? 'Male' : 'Female'; ?></td>
                                        <?php if($data->status == 3): ?>
                                            <th>Delivered</th>
                                        <?php endif; ?>
                                    </tr>
                                </table>
                            </div>

                            <div class="form-group my_right">
                                <button type="button" class="btn btn-info btn-flat" onclick="window.history.back();">Back</button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>