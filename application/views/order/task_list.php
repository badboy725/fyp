<div class="row m-0 mt-2 mb-2">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="bg-light" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <?php if($this->session->userdata('role_keyword') == ROLE_ADMIN) : ?>
                            <th>Full Name</th>
                        <?php endif; ?>
                        <th>Address</th>
                        <th>Total</th>
                        <th>Payment Type</th>
                        <th>Date</th>
                        <th colspan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (!empty($list)) :
                            foreach ($list as $key => $value) :
                    ?>
                            <tr>
                                <td><?php echo $key+1; ?></td>
                                <?php if($this->session->userdata('role_keyword') == ROLE_ADMIN) : ?>
                                    <td><?php echo $value->fullname; ?></td>
                                <?php endif; ?>
                                <td><?php echo $value->address; ?></td>
                                <td><?php echo number_format($value->total_payment, 2); ?></td>
                                <td><?php echo $value->payment_type; ?></td>
                                <td><?php echo $value->order_date; ?></td>
                                <td>
                                    <a href="<?php echo site_url("order/receipt/{$value->order_id}"); ?>" class="btn btn-sm btn-info btn-flat" title="View">View</a>
                                    <a href="<?php echo site_url("task/delivered/{$value->order_id}") ?>" class="btn btn-sm btn-warning btn-flat" title="Delivered">Delivered</a>
                                </td>
                            </tr>
                    <?php
                            endforeach;
                        else :
                    ?>
                        <tr>
                            <td colspan="6" class="text-center">No Order Found</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>