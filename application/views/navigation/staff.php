<?php $nav_active = $this->uri->segment(1); ?>

<nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
    <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="<?php echo site_url(); ?>">Online Restaurant</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item px-lg-4 <?php echo $nav_active == '' ? 'active' : ''; ?>">
                    <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url(); ?>">Home</a>
                </li>

                <li class="nav-item px-lg-4 <?php echo $nav_active == 'task' ? 'active' : ''; ?>">
                    <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('task'); ?>">View Task</a>
                </li>

                <li class="nav-item px-lg-4 <?php echo $nav_active == 'order' ? 'active' : ''; ?>">
                    <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('order'); ?>">Delivery History</a>
                </li>

                <li class="nav-item px-lg-4 <?php echo $nav_active == 'profile' ? 'active' : ''; ?>">
                    <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('profile'); ?>">Profile</a>
                </li>

                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('logout'); ?>">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>