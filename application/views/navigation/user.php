<?php $nav_active = $this->uri->segment(1); ?>

<nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
    <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="<?php echo site_url(); ?>">Online Restaurant</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item px-lg-4 <?php echo $nav_active == '' ? 'active' : ''; ?>">
                    <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url(); ?>">Home</a>
                </li>

                <?php if (empty($this->session->userdata('user_id'))) : ?>
                    <li class="nav-item px-lg-4 <?php echo $nav_active == 'register' ? 'active' : ''; ?>">
                        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('register'); ?>">Sign Up</a>
                    </li>

                    <li class="nav-item px-lg-4 <?php echo $nav_active == 'login' ? 'active' : ''; ?>">
                        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('login'); ?>">Sign In</a>
                    </li>
                <?php endif; ?>

                <li class="nav-item px-lg-4 <?php echo $nav_active == 'menu' ? 'active' : ''; ?>">
                    <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('menu'); ?>">Menu</a>
                </li>

                <?php if (!empty($this->session->userdata('user_id'))) : ?>
                    <li class="nav-item px-lg-4 <?php echo $nav_active == 'shopping_cart' ? 'active' : ''; ?>">
                        <?php $total = count($this->cart->contents()); ?>
                        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('shopping_cart'); ?>">Shopping cart <span class="badge badge-info <?php echo empty($total) ? 'my_hidden' : ''; ?>" id="cart_badge"><?php echo $total; ?></span></a>
                    </li>

                    <li class="nav-item px-lg-4 <?php echo $nav_active == 'order' ? 'active' : ''; ?>">
                        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('order'); ?>">Order History</a>
                    </li>

                    <li class="nav-item px-lg-4 <?php echo $nav_active == 'profile' ? 'active' : ''; ?>">
                        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('profile'); ?>">Profile</a>
                    </li>

                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="<?php echo site_url('logout'); ?>">Logout</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>