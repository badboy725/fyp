<form role="form" method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-5">
            <div class="form-group <?php echo form_has_error('fullname'); ?>">
                <label for="fullname">Staff Name</label>
                <input type="text" class="form-control <?php echo form_has_error('fullname'); ?>" name="fullname" id="fullname" value="<?php echo set_value('fullname', @$data->fullname); ?>" />
                <?php echo form_error_label('fullname'); ?>
            </div>

            <div class="form-group <?php echo form_has_error('handphone_number'); ?>">
                <label for="handphone_number">Contact Number</label>
                <input type="text" class="form-control <?php echo form_has_error('handphone_number'); ?>" name="handphone_number" id="handphone_number" value="<?php echo set_value('handphone_number', @$data->handphone_number); ?>" />
                <?php echo form_error_label('handphone_number'); ?>
            </div>

            <div class="form-group <?php echo form_has_error('email'); ?>">
                <label for="email">Email</label>
                <input type="text" class="form-control <?php echo form_has_error('email'); ?>" name="email" id="email" value="<?php echo set_value('email', @$data->email); ?>" />
                <?php echo form_error_label('email'); ?>
            </div>
            <div class="form-group <?php echo form_has_error('gender'); ?>">
                        <label for="gender">Gender</label>
                        <select class="form-control <?php echo form_has_error('gender'); ?>" id="gender" name="gender">
                            <option value="">Please Select</option>
                            <option value="F" <?php echo set_select('gender', 'F'); ?>>Female</option>
                            <option value="M" <?php echo set_select('gender', 'M'); ?>>Male</option>
                        </select>
                        <?php echo form_error_label('gender'); ?>
                    </div>

            <div class="form-group my_right">
                <button type="submit" class="btn btn-info">Save</button>
                <button type="button" class="btn btn-warning" onclick="window.location.href='<?php echo site_url('staff'); ?>'">Back</button>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function() {
        $('#handphone_number').keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        });
    });
</script>