<div class="row" style="margin: 15px; padding: -10px;">
    <div class="col-lg-3"></div>
    <div class="col-lg-6 my_bg">
        <form role="form" method="post" autocomplete="off">
            <h4 class="text-center" style="padding-top: 10px;">
                Registration Form
            </h4>

            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group <?php echo form_has_error('fullname'); ?>">
                        <label for="fullname">Full Name</label>
                        <input type="text" class="form-control <?php echo form_has_error('fullname'); ?>" name="fullname" id="fullname" placeholder="Full Name" value="<?php echo set_value('fullname', $data->fullname); ?>" />
                        <?php echo form_error_label('fullname'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('email'); ?>">
                        <label for="email">Email</label>
                        <input type="email" class="form-control <?php echo form_has_error('email'); ?>" name="email" id="email" placeholder="Email" value="<?php echo set_value('email', $data->email); ?>" />
                        <?php echo form_error_label('email'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('handphone_number'); ?>">
                        <label for="handphone_number">Handphone Number</label>
                        <input type="text" class="form-control <?php echo form_has_error('handphone_number'); ?>" name="handphone_number" id="handphone_number" placeholder="Handphone Number" value="<?php echo set_value('handphone_number', $data->handphone_number); ?>" />
                        <?php echo form_error_label('handphone_number'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('gender'); ?>">
                        <label for="gender">Gender</label>
                        <select class="form-control <?php echo form_has_error('gender'); ?>" id="gender" name="gender">
                            <option value="">Please Select</option>
                            <option value="F" <?php echo set_select('gender', 'F', 'F' == $data->gender); ?>>Female</option>
                            <option value="M" <?php echo set_select('gender', 'M', 'M' == $data->gender); ?>>Male</option>
                        </select>
                        <?php echo form_error_label('gender'); ?>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group <?php echo form_has_error('username'); ?>">
                        <label for="username">Username</label>
                        <input type="text" class="form-control <?php echo form_has_error('username'); ?>" name="username" id="username" placeholder="Username" value="<?php echo set_value('username'); ?>" />
                        <?php echo form_error_label('username'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('password'); ?>">
                        <label for="password">Password</label>
                        <input type="password" class="form-control <?php echo form_has_error('password'); ?>" name="password" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>" />
                        <?php echo form_error_label('password'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('confirmation_password'); ?>">
                        <label for="confirmation_password">Confirmation Password</label>
                        <input type="password" class="form-control <?php echo form_has_error('confirmation_password'); ?>" name="confirmation_password" id="confirmation_password" placeholder="Confirmation Password" value="<?php echo set_value('confirmation_password'); ?>" />
                        <?php echo form_error_label('confirmation_password'); ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-info btn-block">Submit</button>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#handphone_number').keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        });
    });
</script>