<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Contact Number</th>
            <th>Email Address</th>
            <th>Gender</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(!empty($list)) : 
                foreach($list as $key => $value) :
        ?>
                    <tr>
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $value->fullname; ?></td>
                        <td><?php echo $value->handphone_number; ?></td>
                        <td><?php echo $value->email; ?></td>
                        <td><?php echo $value->gender == 'M' ? 'Male' : 'Female';?></td>
    
                        <td>
                            <a href="<?php echo site_url("staff/form/{$value->user_id}"); ?>" class="btn btn-sm btn-info btn-flat" title="Edit">Edit</a>&nbsp;
                            <a href="<?php echo site_url("staff/delete/{$value->user_id}"); ?>" class="btn btn-sm btn-danger btn-flat" title="Delete" onclick="return delete_staff();">Delete</a>
                        </td>
                    </tr>
        <?php
                endforeach;
            else:
        ?>
            <tr>
                <td colspan="6" class="text-center">No Data Found</td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>

<script>
    function delete_staff() {
        return window.confirm('Are you you want to delete?\nOnce delete this record cannot be recover.');
    }
</script>