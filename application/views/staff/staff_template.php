<div class="row m-0 mt-2 mb-2">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="bg-light" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
            <ul class="nav nav-tabs">
                <li class="my_tab <?php echo $tab_active == 1 ? 'my_nav_active' : ''; ?>">
                    <a href="<?php echo site_url('staff'); ?>">Staff List</a>
                </li>
                <li class="my_tab <?php echo $tab_active == 2 ? 'my_nav_active' : ''; ?>">
                    <a href="<?php echo !empty($data) ? '#' : site_url('staff/form'); ?>"><?php echo !empty($data) ? $data->fullname : 'New Staff'; ?></a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active">
                    <?php echo $tab_view; ?>
                </div>
            </div>
        </div>
    </div>
</div>