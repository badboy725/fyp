<?php $data = $this->session->userdata(); ?>
<div class="row" style="margin: 15px; padding: -10px;">
    <div class="col-lg-2"></div>
    <div class="col-lg-8 my_bg">
        <div class="row">
            <div class="col-lg-6">
                <form role="form" method="post" autocomplete="off">
                    <h4 class="text-center" style="padding-top: 10px;">
                        <strong>Personal Information</strong>
                    </h4>

                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $data['user_id']; ?>" />

                    <div class="form-group <?php echo form_has_error('fullname'); ?>">
                        <label for="fullname">Full Name</label>
                        <input type="text" class="form-control" name="fullname" id="fullname" value="<?php echo set_value('fullname', $data['fullname']); ?>" />
                        <?php echo form_error_label('fullname'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('email'); ?>">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email', $data['email']); ?>" />
                        <?php echo form_error_label('email'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('handphone_number'); ?>">
                        <label for="handphone_number">Handphone Number</label>
                        <input type="text" class="form-control" name="handphone_number" id="handphone_number" value="<?php echo set_value('handphone_number', $data['handphone_number']); ?>" />
                        <?php echo form_error_label('handphone_number'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('gender'); ?>">
                        <label for="gender">Gender</label>
                        <select class="form-control <?php echo form_has_error('gender'); ?>" name="gender" id="gender">
                            <option value="">Please Select</option>
                            <option value="F" <?php echo set_select('gender', 'F', 'F' == $data['gender']); ?>>Female</option>
                            <option value="M" <?php echo set_select('gender', 'M', 'M' == $data['gender']); ?>>Male</option>
                        </select>
                        <?php echo form_error_label('gender'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('personal_password_confirmation'); ?>">
                        <label for="personal_password_confirmation">Password Comfirmation</label>
                        <input type="password" class="form-control" name="personal_password_confirmation" id="personal_password_confirmation" value="<?php echo set_value('personal_password_confirmation'); ?>" />
                        <p class="help-block">Please enter password to update profile.</p>
                        <?php echo form_error_label('personal_password_confirmation'); ?>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-flat" name="update_profile_btn" id="update_profile_btn">Update Profile</button>
                    </div>
                </form>
            </div>

            <div class="col-lg-6">
                <form role="form" method="post" autocomplete="off">
                    <h4 class="text-center" style="padding-top: 10px;">
                        <strong>Change Password</strong>
                    </h4>
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $data['user_id']; ?>" />

                    <div class="form-group <?php echo form_has_error('old_password'); ?>">
                        <label for="old_password">Old Password</label>
                        <input type="password" class="form-control <?php echo form_has_error('old_password'); ?>" name="old_password" id="old_password" value="<?php echo set_value('old_password'); ?>" />
                        <?php echo form_error_label('old_password'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('new_password'); ?>">
                        <label for="new_password">New Password</label>
                        <input type="password" class="form-control <?php echo form_has_error('new_password'); ?>" name="new_password" id="new_password" value="<?php echo set_value('new_password'); ?>" />
                        <?php echo form_error_label('new_password'); ?>
                    </div>

                    <div class="form-group <?php echo form_has_error('confirmation_password'); ?>">
                        <label for="confirmation_password">Confirmation Password</label>
                        <input type="password" class="form-control <?php echo form_has_error('confirmation_password'); ?>" name="confirmation_password" id="confirmation_password" value="<?php echo set_value('confirmation_password'); ?>" />
                        <?php echo form_error_label('confirmation_password'); ?>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-flat" name="change_password_btn" id="change_password_btn">Change Password</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#handphone_number').keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        });
    });

    <?php
        $flash = $this->session->flashdata('message');
        if (!empty($flash)) :
    ?>
            window.alert('<?php echo $flash['message']; ?>');
    <?php endif; ?>
</script>