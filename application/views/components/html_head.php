<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Online Restuarant</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/scss/business-casual.min.css'); ?>" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

    <style>
        .my_bg {
            background-color: darkgrey;
        }

        .my_img {
            height: 300px;
            width: 450px;
        }

        .my_tab {
            padding: 10px 15px;
        }

        .my_nav_active {
            background-color: lightblue;
            color: rgb(200, 0, 0);
            font-weight: bold;
        }

        .my_left {
            float: left;
        }

        .my_right {
            float: right;
        }

        .my_hidden {
            display: none;
        }

        .has-error {
            border-color: rgb(200, 0, 0) !important;
            color: rgb(200, 0, 0);
        }
    </style>
</head>