<div class="row m-0 mt-2 mb-2">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="bg-light" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
            <form role="form" method="post" autocomplete="off">
                <input type="hidden" name="row_id" value="<?php echo $data['rowid']; ?>" />
                <div class="col-lg-12">
                    <table class="table">
                        <tr>
                            <td><label for="product_name">Name</label></td>
                            <td><input type="text" class="form-control" name="product_name" id="product_name" value="<?php echo $data['name']; ?>" disabled /></td>
                        </tr>
                        <tr>
                            <td><label for="product_price">Price</label></td>
                            <td><input type="text" class="form-control" name="product_price" id="product_price" value="<?php echo number_format($data['price'], 2); ?>" disabled /></td>
                        </tr>
                        <tr>
                            <td><label for="quantity">Quantity</label></td>
                            <td><input type="number" class="form-control" name="quantity" id="quantity" min="1" value="<?php echo $data['qty']; ?>" /></td>
                        </tr>
                        <tr>
                            <td><label for="subtotal">Subtotal</label></td>
                            <td><input type="text" class="form-control" name="subtotal" id="subtotal" value="<?php echo number_format($data['subtotal'], 2); ?>" readonly /></td>
                        </tr>
                    </table>

                    <div class="form-group my_right">
                        <button type="submit" class="btn btn-info">Save</button>
                        <button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo site_url('shopping_cart'); ?>'">Back</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#quantity').change(function() {
            var price = parseFloat($('#product_price').val());

            $('#subtotal').val((price * this.value).toFixed(2));
        });
    });
</script>