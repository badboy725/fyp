<div class="row m-0 mt-2 mb-2">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="bg-light" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Price (RM)</th>
                        <th>Quantity</th>
                        <th>SubTotal (RM)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (!empty($list)) :
                            $count = 1;
                            foreach ($list as $key => $value) :
                    ?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $value['name']; ?></td>
                                <td><?php echo number_format($value['price'], 2); ?></td>
                                <td><?php echo $value['qty']; ?></td>
                                <td><?php echo number_format($value['subtotal'], 2); ?></td>
                                <td>
                                    <a href="<?php echo site_url("shopping_cart/edit/{$value['rowid']}"); ?>" class="btn btn-sm btn-info btn-flat" title="Edit">Edit</a>&nbsp;
                                    <a href="<?php echo site_url("shopping_cart/delete/{$value['rowid']}"); ?>" class="btn btn-sm btn-danger btn-flat" title="Delete" onclick="return delete_cart('<?php echo $value['name']; ?>');">Delete</a>
                                </td>
                            </tr>
                    <?php endforeach; ?>
                        <tr>
                            <td></td>
                            <th colspan="3">Total (RM)</th>
                            <td colspan="2"><?php echo number_format($total, 2); ?></td>
                        </tr>
                        <tr>
                            <td colspan="6"><a href="<?php echo site_url('shopping_cart/confirm'); ?>" class="btn btn-success my_right">Confirm</a></td>
                        </tr>
                    <?php else : ?>
                        <tr>
                            <td colspan="6" class="text-center">No Order Found</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    function delete_cart(name) {
        return window.confirm('Are you you want to delete ' + name + ' from cart?');
    }
</script>