<div class="row m-0 mt-2 mb-2">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div class="bg-light" style="height: 600px; overflow-x: hidden; overflow-y: auto;">
            <form role="form" method="post" autocomplete="off" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <table class="table">
                            <tr>
                                <th colspan="4">Order Information</th>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <th>Price (RM)</th>
                                <th>Quantity</th>
                                <th>Subtotal (RM)</th>
                            </tr>
                            <?php foreach ($list as $value) : ?>
                                <tr>
                                    <td><?php echo $value['name']; ?></td>
                                    <td><?php echo number_format($value['price'], 2); ?></td>
                                    <td><?php echo $value['qty']; ?></td>
                                    <td><?php echo number_format($value['subtotal'], 2); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <tr>
                                <th colspan="3" class="text-right">Total (RM)</th>
                                <td><?php echo number_format($total, 2); ?></td>
                            </tr>
                        </table>

                        <div class="form-group <?php echo form_has_error('address'); ?>">
                            <label for="address">Address</label>
                            <textarea class="form-control <?php echo form_has_error('address'); ?>" name="address" id="address"><?php echo set_value('address'); ?></textarea>
                            <?php echo form_error_label('address'); ?>
                        </div>

                        <div class="form-group <?php echo form_has_error('payment_type'); ?>">
                            <label for="payment_type">Payment Type</label>
                            <select class="form-control <?php echo form_has_error('payment_type'); ?>" name="payment_type" id="payment_type">
                                <option value="">Please Select</option>
                                <option value="1" <?php echo set_select('payment_type', 1); ?>>Cash On Delivery</option>
                                <option value="2" <?php echo set_select('payment_type', 2); ?>>Online Banking</option>
                            </select>
                            <?php echo form_error_label('payment_type'); ?>
                        </div>

                        <div class="my_hidden" id="online_banking">
                            <label>Bank Name : XXX Bank</label><br />
                            <label>Bank Account : 123456789</label><br />
                            <label>Bank Account Name : Online Restaurant</label>

                            <div class="form-group <?php echo form_has_error('payment_receipt'); ?>">
                                <label for="payment_receipt">Payment Receipt</label><br />
                                <input type="file" class="<?php echo form_has_error('product_image'); ?>" name="payment_receipt" id="payment_receipt" accept="image/*, .pdf" /><br />
                                <?php echo form_error_label('payment_receipt'); ?>
                            </div>
                        </div>

                        <div class="form-group my_right">
                            <button type="submit" class="btn btn-info">Submit</button>
                            <button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo site_url('shopping_cart'); ?>'">Back</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#payment_type').change(function(){
            if(this.value == 2){
                $('#online_banking').removeClass('my_hidden');
            }else{
                $('#online_banking').addClass('my_hidden');
            }
        });

        <?php
            if(!empty($_POST)) :
                if($this->input->post('payment_type') == 2) :
        ?>
                    $('#online_banking').removeClass('my_hidden');
        <?php 
                endif;
            endif;
        ?>
    });
</script>