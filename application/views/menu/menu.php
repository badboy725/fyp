<div class="row m-0 mt-1 p-1">
    <?php
        if (!empty($list)) :
            foreach ($list as $value) :
    ?>
                <div class="col-lg-1"></div>
                <div class="col-lg-5">
                    <section class="page-section">
                        <div class="container">
                            <div class="product-item">
                                <div class="product-item-title d-flex">
                                    <div class="bg-faded p-3 d-flex rounded">
                                        <span class="section-heading">
                                            <h4 class="section-heading-upper"><?php echo $value->product_caption; ?></h4>
                                            <h2>
                                                <?php echo $value->product_name; ?>
                                                <small>RM <?php echo $value->product_price; ?></small>
                                            </h2>
                                            <button type="button" class="btn btn-info add_product_btn" product_id="<?php echo $value->product_id; ?>">Add to cart</button>
                                        </span>
                                    </div>
                                </div>
                                <img class="my_img" src="<?php echo base_url($value->product_image); ?>" alt="<?php echo $value->product_name; ?>" />
                            </div>
                        </div>
                    </section>
                </div>
    <?php
            endforeach;
        else :
    ?>
        <div class="col-lg-12 text-center">
            <div class="alert alert-danger">Nothing Found</div>
        </div>
    <?php endif; ?>
</div>

<!-- add to cart modal-->
<div class="modal fade" id="add_to_cart_model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add To Cart</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center" id="model_image"></div><br />
                        <input type="hidden" name="product_id" id="product_id" />
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td id="product_name"></td>
                            </tr>
                            <tr>
                                <th>Price (RM)</th>
                                <td id="product_price"></td>
                            </tr>
                            <tr>
                                <th>Quantity</th>
                                <td><input type="number" class="form-control" name="quantity" id="quantity" min="1"></td>
                            </tr>
                            <tr>
                                <th>SubTotal (RM)</th>
                                <td id="subtotal"></td>
                            </tr>
                        </table>

                        <div class="my_right">
                            <button type="button" class="btn btn-info" id="add_to_cart_btn">Add To Cart</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.add_product_btn').click(function() {
            <?php if(empty($this->session->userdata('user_id'))): ?>
                window.alert('Please login first then only make order.');
                window.location.href = '<?php echo site_url('login'); ?>';
            <?php else: ?>
                $.post('<?php echo site_url('product/get_product_details') ?>', {
                    product_id: $(this).attr('product_id')
                }, function(data) {
                    $('#add_to_cart_model #model_image').empty().append('<img src="<?php echo base_url(); ?>' + data.product_image + '" alt="' + data.product_name + '" width="300px" />');

                    $('#add_to_cart_model #product_id').val(data.product_id),
                    $('#add_to_cart_model #product_name').html(data.product_name);
                    $('#add_to_cart_model #product_price').html(data.product_price);
                    $('#add_to_cart_model #quantity').val(1);
                    $('#add_to_cart_model #subtotal').html(data.product_price);

                    $('#add_to_cart_model').modal('show');
                });
            <?php endif; ?>
        });
    });

    $('#add_to_cart_model #quantity').change(function() {
        var price = parseFloat($('#add_to_cart_model #product_price').html());

        $('#add_to_cart_model #subtotal').html((price * this.value).toFixed(2));
    });

    $('#add_to_cart_btn').click(function() {
        $.post('<?php echo site_url('shopping_cart/add') ?>', {
            product_id: $('#add_to_cart_model #product_id').val(),
            product_name: $('#add_to_cart_model #product_name').html(),
            product_price: $('#add_to_cart_model #product_price').html(),
            quantity: $('#add_to_cart_model #quantity').val(),
            subtotal: $('#add_to_cart_model #subtotal').html()
        }, function(data) {
            $('#add_to_cart_model').modal('hide');

            if (data != 0) {
                $('#cart_badge').html(data).removeClass('my_hidden');
                window.alert('Your product is already add to cart.');
            }
        });
    });
</script>