<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('components/html_head'); ?>

<body>
    <h1 class="site-heading text-center text-white d-none d-lg-block">
        <span class="site-heading-upper text-primary mb-3">Online restaurant serve with high quality food</span>
        <span class="site-heading-lower">Feel Hungry Now?</span>
    </h1>

    <?php
        if (empty($this->session->userdata('user_id')) OR $this->session->userdata('role_keyword') == ROLE_USER) {
            $this->load->view('navigation/user');
        } else if($this->session->userdata('role_keyword') == ROLE_ADMIN){
            $this->load->view('navigation/admin');
        } else {
            $this->load->view('navigation/staff');
        }
    ?>

    <?php echo $content; ?>

    <footer class="footer text-faded text-center py-5">
        <div class="container">
            <p class="m-0 small">Copyright &copy; Online Restaurant 2020</p>
        </div>
    </footer>
</body>

</html>