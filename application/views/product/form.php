<form role="form" method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-5">
            <br />
            <?php if (!empty($data)) : ?>
                <div class="form-group text-center">
                    <img src="<?php echo base_url($data->product_image); ?>" width="300px" alt="<?php echo $data->product_name; ?>"/>
                    <input type="hidden" name="product_image_path" value="<?php echo $data->product_image; ?>" />
                </div>
            <?php endif; ?>

            <div class="form-group <?php echo form_has_error('product_name'); ?>">
                <label for="product_name">Product Name</label>
                <input type="text" class="form-control <?php echo form_has_error('product_name'); ?>" name="product_name" id="product_name" value="<?php echo set_value('product_name', @$data->product_name); ?>" />
                <?php echo form_error_label('product_name'); ?>
            </div>

            <div class="form-group <?php echo form_has_error('product_price'); ?>">
                <label for="product_price">Product Price</label>
                <input type="text" class="form-control <?php echo form_has_error('product_price'); ?>" name="product_price" id="product_price" value="<?php echo set_value('product_price', @$data->product_price); ?>" />
                <?php echo form_error_label('product_price'); ?>
            </div>

            <div class="form-group <?php echo form_has_error('product_caption'); ?>">
                <label for="product_caption">Product Caption</label>
                <textarea class="form-control product_caption <?php echo form_has_error('product_caption'); ?>" name="product_caption" id="product_caption" rows="3"><?php echo set_value('product_caption', @$data->product_caption); ?></textarea>
                <?php echo form_error_label('product_caption'); ?>
            </div>

            <div class="form-group <?php echo form_has_error('product_image'); ?>">
                <label for="product_image">Product Image</label>
                <input type="file" class="<?php echo form_has_error('product_image'); ?>" name="product_image" id="product_image" accept="image/*" />
                <?php echo form_error_label('product_image'); ?>
            </div>

            <div class="form-group my_right">
                <button type="submit" class="btn btn-info">Save</button>
                <button type="button" class="btn btn-warning" onclick="window.location.href='<?php echo site_url('product'); ?>'">Back</button>
            </div>
        </div>
    </div>
</form>

<script>
    $(document).ready(function() {
        $('#product_price').keypress(function(e) {
            var charCode = (e.which) ? e.which : e.keyCode;

            return !(charCode > 31 && (charCode < 48 || charCode > 57)) || (charCode == 46 && e.currentTarget.value.indexOf('.') == -1);
        });
    });
</script>