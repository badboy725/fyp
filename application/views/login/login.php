<div class="row" style="margin: 15px; padding: -10px;">
    <div class="col-lg-4"></div>

    <div class="col-lg-4 my_bg" style="padding-bottom: 50px;">
        <div class="text-center">
            <img src="<?php echo base_url('assets/image/avatarred.png'); ?>" style="border-radius: 50%;" width="100px;">
        </div>

        <form role="form" method="post" autocomplete="off"><br>
            <?php
                $flash = $this->session->flashdata('message');
                if (!empty($flash)) :
            ?>
                <div class="alert alert-<?php echo $flash['type']; ?>"><?php echo $flash['message']; ?></div>
            <?php endif; ?>

            <div class="form-group <?php echo form_has_error('username'); ?>">
                <label for="username">Username</label>
                <input type="text" class="form-control <?php echo form_has_error('username'); ?>" name="username" id="username" placeholder="Username" value="<?php echo set_value('username'); ?>" />
                <?php echo form_error_label('username'); ?>
            </div>

            <div class="form-group <?php echo form_has_error('password'); ?>">
                <label for="password">Password</label>
                <input type="password" class="form-control <?php echo form_has_error('password'); ?>" name="password" id="password" placeholder="Password" value="<?php echo set_value('password'); ?>" />
                <?php echo form_error_label('password'); ?>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-info btn-block">Login</button><br>
                <a href="<?php echo site_url('register'); ?>">Don't have an account?</a>
            </div>
        </form>
    </div>
</div>