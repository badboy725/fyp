<?php

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function check_username()
    {
        if ($this->user_model->check_username($this->input->post('username')) === TRUE) {
            $this->form_validation->set_message('check_username', 'This Username has been registered by other user.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    protected function _send_mail(array $email_data): bool
    {
        $this->load->library('email');

        $this->email->from('online_restaurant@online_restaurant.com', 'Online Restaurant');
        $this->email->to($email_data['to']);
        $this->email->cc('lim116@yahoo.com');
        $this->email->subject($email_data['subject']);
        $this->email->message($email_data['message']);

        return $this->email->send();
    }

    protected function _get_hash_password($password)
    {
        return hash('sha1', "A{$password}Z");
    }

    protected function _set_session($user_data)
    {
        $this->session->set_userdata(array(
            'user_id' => $user_data->user_id,
            'username' => $user_data->username,
            'fullname' => $user_data->fullname,
            'email' => $user_data->email,
            'handphone_number' => $user_data->handphone_number,
            'gender' => $user_data->gender,
            'role_keyword' => $user_data->role_keyword
        ));
    }

    protected function _get_upload_file_path($file_name, $config = NULL)
    {
        $file_config = array(
            'upload_path'            => 'uploads/',
            'allowed_types'          => NULL,
            'file_name'              => NULL,
            'file_ext_tolower'       => TRUE,
            'overwrite'              => FALSE,
            'max_size'               => 0,
            'max_width'              => 0,
            'max_height'             => 0,
            'min_width'              => 0,
            'min_height'             => 0,
            'max_filename'           => 0,
            'max_filename_increment' => 100,
            'encrypt_name'           => FALSE,
            'remove_spaces'          => TRUE,
            'detect_mime'            => TRUE,
            'mod_mime_fix'           => TRUE
        );

        if (!empty($config)) {
            foreach ($config as $key => $value) {
                $file_config[$key] = $value;
            }
        }

        $this->load->library('upload', $file_config);

        if ($this->upload->do_upload($file_name)) {
            return "/uploads/{$this->upload->data('file_name')}";
        } else {
            log_message('error', $this->upload->display_errors());
        }
    }
}
