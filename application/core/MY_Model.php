<?php

class MY_Model extends CI_Model
{
    protected $_table_name;
    protected $_primary_key;

    public function __construct()
    {
        parent::__construct();
    }

    public function save(array $data, $id = NULL)
    {
        $return = FALSE;

        if (empty($id)) {
            $result = $this->db->insert($this->_table_name, $data);

            if ($result) {
                $return = $this->db->insert_id();
            }
        } else {
            $return = $this->db->update($this->_table_name, $data, array($this->_primary_key => $id));
        }

        return $return;
    }

    public function delete($primary_key)
    {
        return $this->db->delete($this->_table_name, array($this->_primary_key => $primary_key));
    }

    public function my_dtb($obj, bool $by_having = FALSE): void
    {
        $offset     = $obj['start'];
        $limit      = $obj['length'];
        $columns    = $obj['columns'];
        $order      = isset($obj['order']) ? $obj['order'] : array();
        $search     = $obj['search'];
        $q_group    = FALSE;

        foreach ($columns as $c) {
            if (!empty($c['name'])) {
                if (!empty($search['value'])) {
                    if ($by_having == TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$search['value']}%'");
                    } else {
                        if ($q_group == FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $search['value']);
                    }
                }

                if (!empty($c['search']['value'])) {
                    if ($by_having == TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$c['search']['value']}%'");
                    } else {
                        if ($q_group == FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $c['search']['value']);
                    }
                }
            }
        }

        if ($q_group == TRUE) {
            $this->db->group_end();
        }

        if (!empty($order)) {
            foreach ($order as $o) {
                if (!empty($columns[$o['column']]['name'])) {
                    $this->db->order_by($columns[$o['column']]['name'], $o['dir']);
                }
            }
        }

        $this->db->limit($limit, $offset);
    }

    public function my_dtb_count($obj, bool $by_having = FALSE): void
    {
        $columns = $obj['columns'];
        $search  = $obj['search'];
        $q_group = FALSE;

        foreach ($columns as $c) {
            if (!empty($c['name'])) {
                if (!empty($search['value'])) {
                    if ($by_having == TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$search['value']}%'");
                    } else {
                        if ($q_group == FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $search['value']);
                    }
                }

                if (!empty($c['search']['value'])) {
                    if ($by_having == TRUE) {
                        $this->db->or_having("{$c['name']} LIKE '%{$c['search']['value']}%'");
                    } else {
                        if ($q_group == FALSE) {
                            $this->db->group_start();
                            $q_group = TRUE;
                        }

                        $this->db->or_like($c['name'], $c['search']['value']);
                    }
                }
            }
        }

        if ($q_group == TRUE) {
            $this->db->group_end();
        }
    }
}
