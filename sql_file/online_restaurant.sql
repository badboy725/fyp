/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.11-MariaDB : Database - online_restaurant
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`online_restaurant` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `online_restaurant`;

/*Table structure for table `tbl_carts` */

DROP TABLE IF EXISTS `tbl_carts`;

CREATE TABLE `tbl_carts` (
  `order_id` int(11) NOT NULL COMMENT 'tbl_order',
  `product_id` int(11) NOT NULL COMMENT 'tbl_product',
  `quantity` int(5) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `tbl_carts_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_orders` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_carts_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `tbl_products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_carts` */

insert  into `tbl_carts`(`order_id`,`product_id`,`quantity`,`subtotal`) values 
(6,5,2,10.80),
(7,5,2,10.80),
(8,5,2,10.80);

/*Table structure for table `tbl_orders` */

DROP TABLE IF EXISTS `tbl_orders`;

CREATE TABLE `tbl_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `total_payment` decimal(5,2) NOT NULL,
  `address` text NOT NULL,
  `payment_type` tinyint(1) NOT NULL,
  `payment_receipt` varchar(255) DEFAULT NULL,
  `order_date` datetime NOT NULL,
  `staff_user_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `staff_user_id` (`staff_user_id`),
  CONSTRAINT `tbl_orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_orders_ibfk_2` FOREIGN KEY (`staff_user_id`) REFERENCES `tbl_users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_orders` */

insert  into `tbl_orders`(`order_id`,`user_id`,`total_payment`,`address`,`payment_type`,`payment_receipt`,`order_date`,`staff_user_id`,`status`) values 
(6,5,10.80,'11 taman',1,NULL,'2020-02-02 18:40:33',7,3),
(7,5,10.80,'855645641',1,NULL,'2020-02-03 14:56:27',7,3),
(8,2,10.80,'11taman',1,NULL,'2020-02-04 12:44:54',NULL,1);

/*Table structure for table `tbl_products` */

DROP TABLE IF EXISTS `tbl_products`;

CREATE TABLE `tbl_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_price` decimal(5,2) NOT NULL,
  `product_caption` varchar(200) NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_products` */

insert  into `tbl_products`(`product_id`,`product_name`,`product_image`,`product_price`,`product_caption`) values 
(5,'Mee Goreng','/uploads/mee_goreng.jpg',5.40,'DELICIOUS TREATS, GOOD EATS'),
(6,'Meehoon Goreng','/uploads/meehoon_goreng.jpg',6.50,'GOOD TO EAT'),
(7,'Lava Cake','/uploads/lava_cake.jpg',7.00,'MOST PREFER DESSERT'),
(8,'Creamie Coffe & Cookie','/uploads/creamie_coffe_cookie.jpg',9.90,'DELICIOUS TREATS, GOOD EATS'),
(9,'Nasi Lemak','/uploads/nasi_lemak.jpeg',9.50,'Most delicious dish');

/*Table structure for table `tbl_roles` */

DROP TABLE IF EXISTS `tbl_roles`;

CREATE TABLE `tbl_roles` (
  `role_keyword` varchar(10) NOT NULL,
  `role_description` varchar(100) NOT NULL,
  PRIMARY KEY (`role_keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_roles` */

insert  into `tbl_roles`(`role_keyword`,`role_description`) values 
('admin','Administrator'),
('staff','Staff'),
('user','User');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `handphone_number` varchar(11) NOT NULL,
  `gender` char(1) NOT NULL,
  `role_keyword` varchar(10) NOT NULL COMMENT 'tbl_roles',
  PRIMARY KEY (`user_id`),
  KEY `tbl_users_ibfk_1` (`role_keyword`),
  CONSTRAINT `tbl_users_ibfk_1` FOREIGN KEY (`role_keyword`) REFERENCES `tbl_roles` (`role_keyword`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`user_id`,`username`,`password`,`fullname`,`email`,`handphone_number`,`gender`,`role_keyword`) values 
(1,'admin','922569135331794390ea969b2beac7702df32986','Admin','admin@admin.com','0123456789','M','admin'),
(2,'user','0c0d39bf859730649aa110ddeddfc958e3565db9','User','user@user.com','0123456789','M','user'),
(5,'teckzhi725','027b979fe11cf17f7a34a5fd806447de30839c58','TanTeckZhi','teckzhitan@gmail.com','0169933197','F','user'),
(6,'chai','922569135331794390ea969b2beac7702df32986','chai ja son','chaijason@gmail.com','0169933197','F','user'),
(7,'staff','0c0d39bf859730649aa110ddeddfc958e3565db9','tanwew123','teckzhitan@gmail.com','0169933197','M','staff');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
